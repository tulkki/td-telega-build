#!/usr/bin/env bash
TDIR=$HOME/telega
TDDIR=$HOME/telega/td

if [ -d "$TDDIR" ]; then
  cd "$TDIR" || exit
  git clone https://github.com/zevlg/telega.el.git
  cd "${TDIR}/telega.el" || exit

  # check in telega.el README which is the branch for stable build

  # I got an error using the branch releases 
  # git checkout releases


  #  ## Error (use-package): telega/:config: Symbol’s value as variable is
  #  ##   void: root
  #  ##   Warning (emacs): TDLib version=1.7.5 > 1.7.0 (max required),
  #  ##   please downgrade TDLib and recompile ‘telega-server’                   

  # switching to master fixed the problem
  git branch -a

  make compile
  # now edit your init.el and add telega.el configuration

  # old way
  #make && make install && make test
else
  echo "you need to have tdlib first run buildTd.sh first"
fi
