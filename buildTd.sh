#!/usr/bin/env bash
TDIR=$HOME/telega
# tdlib_version=v1.8.42 https://github.com/zevlg/telega.el/raw/refs/heads/master/etc/Dockerfile
TDLIB_COMMIT="2be9e799a"

if [ -d "$TDIR" ]; then
  cd "$TDIR" || exit
  git clone https://github.com/tdlib/td.git
  cd "${TDIR}/td" || exit
  git checkout "${TDLIB_COMMIT}"
  mkdir build && cd build && cmake ../
  #$ make -jN
  make -j10

  # add sudo permission for this:
  sudo /usr/bin/make install

  ## check if you have this file as root
  #echo "/usr/local/lib" > /etc/ld.so.conf.d/usr_local_lib.conf
  sudo /usr/sbin/ldconfig
else
  mkdir -pv "$TDIR"
  cd "$TDIR" || exit
  git clone https://github.com/tdlib/td.git
  cd "${TDIR}/td" || exit
  git checkout "${TDLIB_COMMIT}"
  mkdir build && cd build && cmake ../
  #$ make -jN
  make -j10

  # add sudo permission for this:
  sudo /usr/bin/make install

  ## check if you have this file as root
  #echo "/usr/local/lib" > /etc/ld.so.conf.d/usr_local_lib.conf
  sudo /usr/sbin/ldconfig
fi
